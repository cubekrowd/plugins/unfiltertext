package net.cubekrowd.unfiltertext;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.regions.Region;
import java.util.ArrayList;
import java.util.Locale;
import net.kyori.adventure.text.Component;
import org.bukkit.Tag;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class UnfilterTextPlugin extends JavaPlugin {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players can use this command");
        } else {
            var player = BukkitAdapter.adapt((Player) sender);
            var session = WorldEdit.getInstance().getSessionManager().get(player);
            var selWorld = session.getSelectionWorld();
            if (selWorld == null) {
                sender.sendMessage("§cNo selection");
            } else {
                Region selection;
                try {
                    selection = session.getSelection(selWorld);
                } catch (IncompleteRegionException e) {
                    sender.sendMessage("§cNo selection");
                    return true;
                }

                var world = BukkitAdapter.adapt(selWorld);
                int findCount = 0;

                for (var pos : selection) {
                    var block = world.getBlockAt(pos.getBlockX(), pos.getBlockY(), pos.getBlockZ());
                    if (Tag.SIGNS.isTagged(block.getType())) {
                        findCount++;

                        var sign = (Sign) block.getState();
                        var originalLines = new ArrayList<>(sign.lines());
                        // @NOTE(traks) setting the line also sets the
                        // FilterText# tag. When the game saves the sign it
                        // checks if the filter text and actual text match. If
                        // they do match, it doesn't save the filtered text. So
                        // this effectively clears the filtered text.

                        // @NOTE(traks) first need to clear the lines, because
                        // Paper doesn't update the lines on the NMS block if
                        // the new lines equal the old lines
                        for (int i = 0; i < originalLines.size(); i++) {
                            sign.line(i, Component.text("@clear"));
                        }

                        sign.update();

                        sign = (Sign) block.getState();

                        for (int i = 0; i < originalLines.size(); i++) {
                            sign.line(i, originalLines.get(i));
                        }

                        sign.update();
                    }
                }

                sender.sendMessage(String.format(Locale.ENGLISH, "Found %s signs", findCount));
            }
        }
        return true;
    }
}
