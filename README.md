# UnfilterText

**Notice**: More recent versions of Paper 1.18 automatically fix issues with FilteredText tags, so this plugin is no longer needed.

In the lobby we were having some issues with signs suddenly being cleared. As it turns out, Mojang introduced a new text filtering feature in 1.16.4 for signs, books, and so on. On single player I believe, this uses some Microsoft account setting. On multiplayer it uses the 'text-filtering-config' string in 'server.properties' (and not the client setting). If this setting is empty, no filtering will be performed.

If filtering is somehow enabled, the server/client will send textual data to whatever host was configured (likely Microsoft) to filter text. The item/block will then store the filtered text along with the unfiltered text. For example, on signs the first line is stored under 'Text1' while the filtered first line is stored under 'FilteredText1'.

Whenever vanilla/Paper/etc. loads a sign with no filtered text, it copies the unfiltered text to the filtered text tags. Whenever it saves a sign, it only saves the filtered text if it differs from the unfiltered text. These two tags can somehow desync (not sure how). If the FilteredText tags become e.g. empty strings, the sign text will appear gone even though the unfiltered text is still stored on the sign block.

It's possible to remove FilteredText tags with `/data remove x y z FilteredText#`, but this is cumbersome to use in bulk. This plugin makes clearing these tags more convenient. Simply select an area with WorldEdit and run `/unfiltertext` to wipe all FilteredText tags on signs. You need the permission `unfiltertext.use` to use this plugin.
